module.exports = {
	"proxy": "https://boxofexpression.local",
	"notify": false,
	"files": ["./css/*.min.css", "./js/*.min.js", "./**/*.php"]
};